# Corda Kotlin Template

To run the Cordapp follow the below steps:

## 1. Build the project

Windows: `gradlew deployNodes`  

Ubuntu: `./gradlew deployNodes`  

## 2. Run the network

Windows: `build\nodes\runnodes.bat`  

Ubuntu: `build/nodes/runnodes`  

## 3. Sample Transaction

Select RBI node console, type the following command to issue 100 tokens:

`start IssueTokenFlow currency: INR, amount: 100, recipient: SBI`

Select SBI node console, type the following command to move 30 tokens:

`start moveToken recipient: PNB, currency: INR, amount: 30, issuer: RBI`