package com.template.flows

import co.paralleluniverse.fibers.Suspendable
import com.r3.corda.lib.tokens.contracts.states.FungibleToken
import com.r3.corda.lib.tokens.contracts.types.IssuedTokenType
import com.r3.corda.lib.tokens.contracts.utilities.heldBy
import com.r3.corda.lib.tokens.contracts.utilities.issuedBy
import com.r3.corda.lib.tokens.contracts.utilities.of
import com.r3.corda.lib.tokens.workflows.flows.move.MoveFungibleTokensFlow
import com.r3.corda.lib.tokens.workflows.flows.move.MoveTokensFlowHandler
import com.r3.corda.lib.tokens.workflows.flows.rpc.IssueTokens
import com.r3.corda.lib.tokens.workflows.types.PartyAndAmount
import com.r3.corda.lib.tokens.workflows.utilities.tokenAmountWithIssuerCriteria
import com.template.types.ExampleFixedTokenType
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.utilities.ProgressTracker

@StartableByRPC
class IssueTokenFlow(val currency: String, val amount: Long, val recipient: Party) : FlowLogic<SignedTransaction>() {
    override val progressTracker = ProgressTracker()

    @Suspendable
    override fun call(): SignedTransaction {
        val initiator = ourIdentity
        val holder = recipient
        val token = ExampleFixedTokenType.getInstance(currency)
        // Starts a new flow session.

        val myIssuedTokenType: IssuedTokenType = token issuedBy initiator
        val myissuedtoken = amount of myIssuedTokenType
        val fungibletoken: FungibleToken = myissuedtoken heldBy holder
        return subFlow(IssueTokens(listOf(fungibletoken)))
    }

}

@StartableByRPC
@InitiatingFlow
class MoveToken (val recipient: Party, val currency: String, val amount: Int, val issuer: Party): FlowLogic<SignedTransaction>(){
    override val progressTracker = ProgressTracker()
    @Suspendable
    override fun call(): SignedTransaction {
        val holder = recipient
        val otherHolder = ourIdentity
        val initiator = issuer
        val mytoken= ExampleFixedTokenType.getInstance(currency)
        val holderSession = initiateFlow(holder)
        val otherHolderSession = initiateFlow(otherHolder)
        return subFlow(MoveFungibleTokensFlow(
                partyAndAmount = PartyAndAmount(holder,amount of mytoken),
                participantSessions = listOf(holderSession, otherHolderSession),
                queryCriteria = tokenAmountWithIssuerCriteria(mytoken, initiator)))
    }
}
@InitiatedBy(MoveToken::class)
class MoveTokenFlowResponder (private val othersession: FlowSession): FlowLogic<Unit>(){
    @Suspendable
    override fun call() {
        subFlow(MoveTokensFlowHandler(othersession))
    }
}